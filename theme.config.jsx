export default {
    useNextSeoProps() {
        return {
            titleTemplate: '%s – Paye Ton Kawa'
        }
    },
    logo: <span><i aria-hidden className="fa-solid fa-mug-hot"></i> Paye Ton Kawa</span>,
    project: {
        link: 'https://gitlab.com/maximebaudoin/documentation-nextra'
    },
    docsRepositoryBase: 'https://gitlab.com/maximebaudoin/documentation-nextra/-/tree/main/'
    // ... other theme options
}

